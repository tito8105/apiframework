from rest_framework import serializers
from .models import Telefono
from django.contrib.auth.models import User, Group

class TelefonoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Telefono
        fields = '__all__'

