from django.urls import path,  include
from rest_framework import routers
from core.telefono import views

routers=routers.DefaultRouter()
routers.register(r'telefono', views.TelefonoViewSet)

urlpatterns=[

    path('', include(routers.urls)),
    path('telefono/',include('rest_framework.urls', namespace='rest_framework'))
]

#hola